package com.revolut.transactional.exception;

public class MoneyTransferException extends Exception {

    public MoneyTransferException(String message) {
        super(message);
    }
}
