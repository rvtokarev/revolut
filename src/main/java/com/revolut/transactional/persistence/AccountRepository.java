package com.revolut.transactional.persistence;

import com.revolut.transactional.model.Account;
import org.apache.deltaspike.data.api.EntityPersistenceRepository;
import org.apache.deltaspike.data.api.Repository;

import java.util.Optional;

@Repository
public interface AccountRepository extends EntityPersistenceRepository<Account, Integer>{

    Optional<Account> findBy(Integer id);
}
