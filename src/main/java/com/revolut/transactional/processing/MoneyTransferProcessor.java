package com.revolut.transactional.processing;

import com.revolut.transactional.exception.MoneyTransferException;
import javax.enterprise.context.ApplicationScoped;
import com.revolut.transactional.model.Account;
import com.revolut.transactional.persistence.AccountRepository;
import org.infinispan.factories.annotations.Inject;

import javax.transaction.Transactional;
import java.math.BigDecimal;

@ApplicationScoped
public class MoneyTransferProcessor {

    @Inject
    private AccountRepository personRepository;

    @Transactional
    public void transfer(Integer idFrom, Integer idTo, BigDecimal amount) throws MoneyTransferException {
        if(amount.compareTo(BigDecimal.ZERO) < 0)
            throw new MoneyTransferException("Negative amount");

        Account accountFrom = personRepository.findBy(idFrom).get();

        if(accountFrom.getAmount().compareTo(amount) < 0)
            throw new MoneyTransferException(("Not enouth money"));

        Account accountTo = personRepository.findBy(idTo).get();

        accountFrom.setAmount(accountFrom.getAmount().subtract(amount));
        accountTo.setAmount(accountTo.getAmount().add(amount));
    }
}
