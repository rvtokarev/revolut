package com.revolut.transactional.rest;

import com.revolut.transactional.exception.MoneyTransferException;
import com.revolut.transactional.processing.MoneyTransferProcessor;
import org.infinispan.factories.annotations.Inject;

import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import java.math.BigDecimal;


@Path("/hello")
public class HelloWorldEndpoint {

	@Inject
	MoneyTransferProcessor moneyTransferProcessor;

	@GET
	@Produces("text/plain")
	public Response doGet(@QueryParam("from") Integer idFrom, @QueryParam("to") Integer idTo, @QueryParam("amount")Long amount) throws MoneyTransferException {
		moneyTransferProcessor.transfer(idFrom, idTo, BigDecimal.valueOf(amount));
		return Response.ok("Transfered from " + idFrom + " to " + idTo + " amount: " + amount + ".").build();
	}
}
