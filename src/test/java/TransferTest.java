import com.revolut.transactional.exception.MoneyTransferException;
import com.revolut.transactional.processing.MoneyTransferProcessor;
import jdk.internal.jline.internal.TestAccessible;
import org.infinispan.factories.annotations.Inject;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.math.BigDecimal;

@RunWith(Arquillian.class)
public class TransferTest {

    @Inject
    MoneyTransferProcessor moneyTransferProcessor;

    @Test
    @Ignore
    public void transferTest() throws MoneyTransferException {
        moneyTransferProcessor.transfer(1,2, BigDecimal.valueOf(100));
        moneyTransferProcessor.transfer(1,2, BigDecimal.valueOf(-100));
        moneyTransferProcessor.transfer(2,1, BigDecimal.valueOf(100));
    }
}
